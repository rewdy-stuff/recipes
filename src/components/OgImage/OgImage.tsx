import React from "react";
import { siteTitle } from "../../siteConfig";

const red = "#db0101";
const lightRed = "#ffdddd";

export const OgImageComponent: (
  title?: string,
  description?: string,
) => React.ReactNode = (title = "Yum!", description) => {
  const titleSize = title.length <= 12 ? 180 : title.length > 30 ? 72 : 96;
  const showDescription =
    description && description.length > 0 && title.length < 120;
  return (
    <div
      style={{
        height: "100%",
        width: "100%",
        display: "flex",
        padding: "40px",
        flexDirection: "column",
        flexWrap: "nowrap",
        backgroundColor: "#fff",
        backgroundImage: `radial-gradient(circle at 20px 20px, ${red} 1%, transparent 0%), radial-gradient(circle at 60px 60px, ${red} 1%, transparent 0%)`,
        backgroundSize: "80px 80px",
        borderTop: `12px solid ${red}`,
        fontSize: 20,
        fontWeight: 600,
      }}
    >
      <h1
        style={{
          fontFamily: "Open Sans",
          fontWeight: 800,
          color: red,
          marginBottom: 15,
          marginTop: 10,
        }}
      >
        {siteTitle}
      </h1>
      <div
        style={{
          width: 120,
          height: 12,
          display: "block",
          backgroundColor: lightRed,
        }}
      />
      <div
        style={{
          marginTop: 60,
          fontSize: titleSize,
          lineHeight: 1,
          letterSpacing: "-0.05em",
          flexGrow: 1,
        }}
      >
        {title}
      </div>
      {showDescription && (
        <div
          style={{
            marginTop: 30,
            fontSize: 38,
            lineHeight: 1.5,
            color: "#666",
          }}
        >
          {description}
        </div>
      )}
    </div>
  );
};
