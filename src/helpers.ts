import { getCollection } from "astro:content";
import { orderBy } from "lodash-es";

export const getRecipes = async (limit = 9) => {
  const recipes = await getCollection(
    "recipes",
    ({ data }) => data.published === true,
  );
  const sorted = orderBy(recipes, ["data.date", "data.title"], ["desc", "asc"]);
  return limit ? sorted.slice(0, limit) : sorted;
};
