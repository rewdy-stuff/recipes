import { OgImageComponent } from "../../../components/OgImage/OgImage";
import { getRecipes } from "../../../helpers";
import type { APIRoute } from "astro";
import fs from "fs/promises";
import satori from "satori";
import sharp from "sharp";

export async function getStaticPaths() {
  const recipes = await getRecipes(0);
  return recipes.map((entry) => ({
    params: {
      slug: entry.slug,
      title: entry.data.title,
      date: entry.data.date.toISOString(),
    },
    props: entry,
  }));
}

const WIDTH = 1200;
const HEIGHT = 630;

export const GET: APIRoute = async ({ props }) => {
  const karlaFont = await fs.readFile("./static/fonts/karla/Karla-Bold.ttf");
  const openSansFont = await fs.readFile(
    "./static/fonts/open-sans/OpenSans-ExtraBold.ttf",
  );

  const svg = await satori(
    OgImageComponent(props.data.title, props.data.description),
    {
      width: WIDTH,
      height: HEIGHT,
      fonts: [
        { name: "Karla", data: karlaFont, weight: 600, style: "normal" },
        {
          name: "Open Sans",
          data: openSansFont,
          weight: 800,
          style: "normal",
        },
      ],
    },
  );

  const png = await sharp(Buffer.from(svg)).png().toBuffer();

  return new Response(png, {
    headers: {
      "Content-Type": "image/png",
    },
  });
};
