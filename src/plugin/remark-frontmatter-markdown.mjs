import { marked } from "marked";

const frontmatterKeysToProcess = ["instructions", "notes", "ingredients"];

/**
 * Remark plugin to process some frontmatter keys AS markdown.
 *
 * This is not very resilient. Only works if they key values are strings
 * or lists of strings. If you want to use it for more than that, you'll
 * need to add more logic.
 */
export function remarkFrontmatterMarkdown() {
  return function (_tree, file) {
    frontmatterKeysToProcess.forEach((key) => {
      if (key in file.data.astro.frontmatter) {
        if (Array.isArray(file.data.astro.frontmatter[key])) {
          file.data.astro.frontmatter[key] = file.data.astro.frontmatter[
            key
          ].map((line) => marked.parseInline(line));
        } else if (typeof file.data.astro.frontmatter[key] === "string") {
          file.data.astro.frontmatter[key] = marked.parseInline(
            file.data.astro.frontmatter[key],
          );
        }
      }
    });
  };
}
