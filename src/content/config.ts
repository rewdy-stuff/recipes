import { defineCollection, z } from "astro:content";

const recipeSchema = z.object({
  title: z.string(),
  date: z.date(),
  published: z.boolean(),
  description: z.string(),
  ingredients: z.array(z.string()),
  instructions: z.array(z.string()),
  notes: z.array(z.string()).optional(),
  servings: z.number(),
  servingSize: z.string().optional(),
  wwPoints: z.number().optional(),
  wwRecipeLink: z.string().optional(),
  tags: z.array(z.string()),
  photo: z.string().optional(),
});

const recipeCollection = defineCollection({
  type: "content",
  schema: recipeSchema,
});

export type Recipe = z.infer<typeof recipeSchema>;

export const collections = {
  recipes: recipeCollection,
};
