export const siteTitle = "Rewdy's Recipes";
export const navItems = {
  "/": "Home",
  "/recipes": "Recipes",
  "/about": "About",
};
