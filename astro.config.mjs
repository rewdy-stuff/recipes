import { remarkFrontmatterMarkdown } from "./src/plugin/remark-frontmatter-markdown.mjs";
import mdx from "@astrojs/mdx";
import react from "@astrojs/react";
import { defineConfig } from "astro/config";

// https://astro.build/config
export default defineConfig({
  // Published URL
  site: "https://recipes.rewdy.lol",
  // GL Pages wants the built site in "public"
  outDir: "public",
  // Since we can't use "public" for static assets anymore, we'll move those to "static"
  publicDir: "static",
  integrations: [
    react(),
    mdx({
      remarkPlugins: [remarkFrontmatterMarkdown],
    }),
  ],
});
